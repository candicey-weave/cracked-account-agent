package com.gitlab.candicey.crackedaccountagent

import com.gitlab.candicey.crackedaccountagent.transformer.AccountManagerTransformer
import java.lang.instrument.Instrumentation

object CrackedAccountAgentMain {
    @JvmStatic
    fun premain(arg: String?, inst: Instrumentation) {
        inst.addTransformer(AccountManagerTransformer)
    }
}